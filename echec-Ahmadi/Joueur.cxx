#include <iostream>
#include "Joueur.h"
#include "Piece.h"

Joueur::~Joueur()
{
  //cout << (m_pieces[0]->isWhite() ? "Destruction Joueur Blanc" : "Destruction Joueur Noir") << endl;
  /*
  for (int i=0; i<16; i++)
    delete m_pieces[i];
  */
  vector<Piece*>::iterator p = m_pieces.begin();
  while ( p != m_pieces.end() )
    {
      delete *p;
      p++;
    }
}

JoueurBlanc::JoueurBlanc() //: Joueur(true)
{
  Roi *pr = new Roi(true);
  Reine *pq = new Reine(true);
  Fou *pfl = new Fou(true,true);
  Fou *pfr = new Fou(true,false);
  Tour *ptl = new Tour(true,true);
  Tour *ptr = new Tour(true,false);
  Cavalier *pcl = new Cavalier(true,true);
  Cavalier *pcr = new Cavalier(true,false);

  m_pieces.push_back(pr);
  m_pieces.push_back(pq);
  m_pieces.push_back(pfl);
  m_pieces.push_back(pfr);
  m_pieces.push_back(ptl);
  m_pieces.push_back(ptr);
  m_pieces.push_back(pcl);
  m_pieces.push_back(pcr);

  for (int i=1; i<=8; i++)
    {
      Piece *p = new Piece(i,2,true); // Pions
      m_pieces.push_back(p);
    }
  //cout << "Constructeur Joueur Blanc" << endl;
}

JoueurNoir::JoueurNoir() //: Joueur(false)
{
  Roi *pr = new Roi(false);
  Reine *pq = new Reine(false);
  Fou *pfl = new Fou(false,true);
  Fou *pfr = new Fou(false,false);
  Tour *ptl = new Tour(false,true);
  Tour *ptr = new Tour(false,false);
  Cavalier *pcl = new Cavalier(false,true);
  Cavalier *pcr = new Cavalier(false,false);

  m_pieces.push_back(pr);
  m_pieces.push_back(pq);
  m_pieces.push_back(pfl);
  m_pieces.push_back(pfr);
  m_pieces.push_back(ptl);
  m_pieces.push_back(ptr);
  m_pieces.push_back(pcl);
  m_pieces.push_back(pcr);

  for (int i=1; i<=8; i++)
    {
      Piece *p = new Piece(i,7,false); // Pions
      m_pieces.push_back(p);
    }
  //cout << "Constructeur Joueur Noir" << endl;
}

Joueur::Joueur()
{
  //cout << "Constructeur Joueur par defaut" << endl;
}

Joueur::Joueur(bool white)
{
  /*
  int n = 0;
  int y = white ? 1 : 8;
  for ( int x = 1; x <= 8; ++x )
    {
      m_pieces[ n ].init( x, y, white );
      n=n+1;
    }
  y = white ? 2 : 7;
  for ( int x = 1; x <= 8; ++x )
    m_pieces[ n++ ].init( x, y, white );
  */
  //cout << "Constructeur Joueur specialise" << endl;
}

// n++ : retourner n puis n=n+1
// ++n : n=n+1 puis retourner n

void
Joueur::placerPieces(Echiquier &e)
{
  for (int i=0; i<16; i++)
    e.placer( m_pieces[i] );
}

void
Joueur::affiche()
{
  for (int i=0; i<16; i++)
    m_pieces[i]->affiche();
}

bool
JoueurBlanc::isWhite()
{
  return true;
}

bool
JoueurNoir::isWhite()
{
  return false;
}
