/**
 * Programme principal
 *
 * @file principal.cxx
 */

// Utile pour l'affichage
#include <iostream>
#include <string>
#include <cstdlib>
#include "Piece.h"
#include "Joueur.h"
#include "Echiquier.h"

// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;

void afficherQuiJou(bool isLeTourDesWhite, bool isErreur)
{
    cout << (isErreur ? "ERREUR : " : "") << "C'est au tour des " << (isLeTourDesWhite ?  "blans" : "noirs") << " de jouer" << endl;
}

/**
 * Programme principal
 */
int main( int argc, char** argv )
{
    JoueurBlanc jb;
    JoueurNoir jn;

    //jb.affiche();
    //jn.affiche();

    Echiquier e;


    jb.placerPieces(e);
    jn.placerPieces(e);

    /*Piece* unP = e.getPiece(3,1);
    if(unP->mouvementValide(e,6,5))
    {
      e.deplacer(unP, 6,5);
    }*/
    bool finPartie(false);
    bool isLeTourDesWhite = false;
    e.affiche();
    cout << "================== Debut de la partie ===============" << endl;
    afficherQuiJou(isLeTourDesWhite, false);
    do
    {
        cout << "Choisissez une piece (entrez les coordonnees de la piece sous format xy ) ou saisissez 'Q' pour quitter" << endl;
        string coordonnees;
        cin >> coordonnees;
        if(coordonnees == "Q" || coordonnees == "q")
        {
            cout << "========= Partie fini ==============";
            break;
        }

        if (coordonnees.length() == 2)
        {
            char charX = (char)coordonnees[0];
            char charY = (char)coordonnees[1];
            // conversion des char en int
            int x = charX-'0';
            int y = charY-'0';

            Piece* pieceSelectionne = e.getPiece(x,y);
            if(pieceSelectionne != NULL)
            {
                if(pieceSelectionne->isWhite() == isLeTourDesWhite)
                {
                    cout << "Choisissez la case de destination (format : xy )" << endl;
                    cin >> coordonnees;
                    charX = (char)coordonnees[0];
                    charY = (char)coordonnees[1];
                    x = charX-'0';
                    y = charY-'0';
                    if(pieceSelectionne->mouvementValide(e,x,y))
                    {
                        if(e.deplacer(pieceSelectionne, x,y))
                        {
                            system("cls");

                            e.affiche();
                            isLeTourDesWhite = !isLeTourDesWhite;
                            afficherQuiJou(isLeTourDesWhite, false);
                        }
                        else
                        {
                            system("cls");
                            e.affiche();
                            cout << "ERREUR : Deplacement non autorise (case occupee, coordonnees invalides, piece vide )" << endl;

                        }
                    }
                    else
                    {
                        system("cls");
                        e.affiche();
                        cout << "ERREUR : Mouvement non autorise" << endl;
                    }
                }
                else
                {
                    system("cls");
                    e.affiche();
                    afficherQuiJou(isLeTourDesWhite, true);
                }
            }
            else
            {
                system("cls");
                e.affiche();
                cout << "Veuillez selectionner une piece valide" << endl;
            }
        }
        else
        {
            system("cls");
            e.affiche();
            cout << "ERREUR : Erreur de saisie" << endl;

        }
    }
    while(!finPartie);

}
